import UIKit

//Without optional
//let str: String = nil
//print(str)

//Wit optional
//let str: String? = nil
//print(str!)

var myString:String? = nil
myString = "venkat"
myString = nil
//print(myString!) // it won't become nil

if myString != nil { //explicitly chack
    print(myString!)
}
