//
//  ViewController.swift
//  Dicee
//
//  Created by VENKATESH on 29/03/20.
//  Copyright © 2020 VENKATESH. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var diceeImage1: UIImageView!
    @IBOutlet weak var diceeImage2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func rollAction(_ sender: UIButton) {
        //diceeImage1.image = #imageLiteral(resourceName: "DiceTwo")
        //diceeImage2.image = #imageLiteral(resourceName: "DiceSix")
        let diceeArray = [#imageLiteral(resourceName: "DiceOne"), #imageLiteral(resourceName: "DiceTwo"), #imageLiteral(resourceName: "DiceThree"), #imageLiteral(resourceName: "DiceFour"), #imageLiteral(resourceName: "DiceFive"), #imageLiteral(resourceName: "DiceSix")]
        diceeImage1.image = diceeArray[Int.random(in: 0...5)]
        diceeImage2.image = diceeArray[Int.random(in: 0...5)]
    }
    
}

