//
//  Structs.swift
//  GV_SwiftStoryboard
//
//  Created by VENKATESH on 14/04/20.
//  Copyright © 2020 VENKATESH. All rights reserved.
//

import Foundation

struct Question {
    let text: String
    let answer: String
    
    init(q: String, a: String) {
        text = q
        answer = a
    }
}
