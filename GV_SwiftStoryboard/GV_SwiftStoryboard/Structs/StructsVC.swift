//
//  StructsViewController.swift
//  GV_SwiftStoryboard
//
//  Created by VENKATESH on 14/04/20.
//  Copyright © 2020 VENKATESH. All rights reserved.
//

import UIKit

class StructsVC: UIViewController {
    
    let quiz = [
                Question(q: "4 + 5 = 9", a: "True"),
                Question(q: " 4 - 1 = 3", a: "True"),
                Question(q: "3 - 2 = 3", a: "False"),
    ]
    
    var questionNumber = 0

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    @IBAction func trueAction(_ sender: UIButton) {
        
        NSLog(sender.currentTitle!)
        let userAns = sender.currentTitle
        let actualAns = quiz[questionNumber].answer

        if userAns == actualAns {
            NSLog("TRUE")
            sender.backgroundColor = UIColor.green
        }else{
            NSLog("FALSE")
            sender.backgroundColor = UIColor.red
        }

        NSLog("\(questionNumber)")
        if (questionNumber < quiz.count - 1) {
            questionNumber += 1
        }
        else
        {
            questionNumber = 0
        }
        
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateUI), userInfo: nil, repeats: false)
    }
    
    @IBAction func FalseAction(_ sender: UIButton) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    @objc func updateUI () {
        questionLabel.text = quiz[questionNumber].text
        progressBar.progress = Float(questionNumber + 1) / Float(quiz.count)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
