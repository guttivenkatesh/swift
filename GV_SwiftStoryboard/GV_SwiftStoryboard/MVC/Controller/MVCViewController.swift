//
//  MVCViewController.swift
//  GV_SwiftStoryboard
//
//  Created by VENKATESH on 14/04/20.
//  Copyright © 2020 VENKATESH. All rights reserved.
//

import UIKit

class MVCViewController: UIViewController {
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var answerButton: UIButton!
    
    var quizBrain = QuizBrain()
    
    
    @IBAction func answerButtonPressed(_ sender: UIButton) {
            
        let userAnswer = sender.currentTitle!
        let userGotItRight = quizBrain.checkAnswer(userAnswer)
        
        if userGotItRight {
            sender.backgroundColor = UIColor.green
        }else
        {
            sender.backgroundColor = UIColor.red
        }
        
        quizBrain.nextQuestion()
        
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateUI), userInfo: nil, repeats: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateUI()
    }
    
    @objc func updateUI() {
        questionLabel.text = quizBrain.getQuestionText()
        progressView.progress = quizBrain.getProgress()
        
        answerButton.backgroundColor = UIColor.clear
    }
    
    
    }
