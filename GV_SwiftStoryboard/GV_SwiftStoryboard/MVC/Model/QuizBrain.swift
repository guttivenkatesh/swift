//
//  QuizBrain.swift
//  GV_SwiftStoryboard
//
//  Created by VENKATESH on 14/04/20.
//  Copyright © 2020 VENKATESH. All rights reserved.
//

import Foundation

struct QuizBrain {
    let quiz = [
                Question(q: "4 + 5 = 9", a: "True"),
                Question(q: " 4 - 1 = 3", a: "True"),
                Question(q: "3 - 2 = 3", a: "False"),
    ]
    
    var questionNumber = 0
    
//    func checkAnswer(userAnswer: String) {
//        print(userAnswer)
//    }
    
    //if your getting any confusion for attribute param, use external param "answer" and inside use internal param "userAnswer"
    // calling time use: checkAnswer(answer: "pass Answer")
//    func checkAnswer(answer userAnswer: String) {
//        print(userAnswer)
//    }
    
    //use external w/o name, calling time directly call: checkAnswer("pass Answer")
    func checkAnswer(_ userAnswer: String) -> Bool{
        
        if userAnswer == quiz[questionNumber].answer {
            return true
        }
        else
        {
            return false
        }
    }
    
    func getQuestionText() -> String {
        return quiz[questionNumber].text
    }
    
    func getProgress() -> Float {
        let progress = questionNumber / quiz.count
        return Float(progress)
    }
    
    mutating func nextQuestion() {
        if questionNumber + 1 < quiz.count {
            questionNumber += 1
        }
        else
        {
            questionNumber = 0
        }
    }
}
