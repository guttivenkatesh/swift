import UIKit

var str = "Hello, playground"

let dict = ["name": "Venkat", "age": "32",]
print(dict["name"]!)

//specify the explicit datatype
let dict1 : [String : Int] = ["age" : 32, "id" : 101]
print(dict1)

let dict2 = ["Name": "Venkat", "age": "32",]
//print(dict2["name"]!) // in key "N" is caps
//maybe sometines bymistake user can do this type of mistakes. So to overcome this type of crashes use below code(optional binding),
if let result = dict2["name"] {
    print(result)
}

 
