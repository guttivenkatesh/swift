import UIKit

var str = "Hello, playground"

//Ex: 1
func greeting1() {
    print("Hello")
}

greeting1()

//Ex: 2
func greeting2(name: String) {
    print("Hello \(name)")
}

greeting2(name: "Venkat")

//Ex: 3
func greeting3(name: String) -> Bool {
    if name == "Venkat" {
        return true
    }else
    {
        return false
    }
}

var doorShouldOpen = greeting3(name: "Vvvv")


//Ex: 4
func getMilk (money: Int) -> Int {
    let change = money - 2
    return change
}

var value = getMilk(money: 5)

//Ex: 5
