import UIKit

var str = "Hello, playground"

//1D-Array
let array = ["venkat", "nani", "Teja"]
print(array)
print(array[0])


let array1 = ["venkat", 32, "Teja"] as [Any]
print(array1)


//2D-Array
let array2 = [
            [1,2,3],
            [4,5,6],
            [7,8,9]
        ]
print(array2[2][2]) //9

let array3 = [
        ["name","Venkat"],
        ["age","32"]
        ]
print(array3[0][1]) //Venkat
