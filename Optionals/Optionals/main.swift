
//Ex: 1
let myOptional: String? //optional string
myOptional = "Venkat" //myOptional is optional string

//if you assign like below, it won't allow because "text" is String and "myOptional" is optional String.
//let text: String = myOptional
//how could we turn it optional
let text: String = myOptional!
//Force unwrap the optional, but this of course requirese really think whether if there is case where we could be unwrapping the optional it could have actually nil value store. In above case i know this is nver gona be nil.


//Ex: 2
//But what if myOptional actually storing nil, in this case XCode is not reporting any error and project build successfully but while running it thrown error.
let myOptional1: String?
myOptional1 = nil
//let text1: String = myOptional!

if myOptional1 != nil {
    let text1: String = myOptional!
}
else
{
    print("myOptional was found to be nil")
}

//Ex: 3
let myOptional2: String?
myOptional2 = "Venkat"

if let safeOPtional = myOptional2 {
    let text2: String = safeOPtional
    let text3: String = safeOPtional
    print(safeOPtional)
}
else
{
    print("myOptional was found to be nil")
}

//Ex: 4
let myOptional3: String?
myOptional3 = nil

let text2: String = myOptional3 ?? "I am the default value"
print(text)

//Ex: 5
struct MyOptional4 {
    var property = 123
    func method() {
        print("i am the struct method")
    }
}
let myOptional5: MyOptional4?
myOptional5 = MyOptional4()
print(myOptional5!.property)

let myOptional6: MyOptional4?
myOptional6 = nil
print(myOptional6?.property)
