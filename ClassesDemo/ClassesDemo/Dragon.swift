
class Dragon: Enemy {
    var wingsSpan = 2
    
    func talk(speech: String) {
        print("speech \(speech)")
    }
    
    override func move() {
        print("Fly forwards")
    }
}
