import UIKit

var str = "Hello, playground"

let array3 = [
        ["name","Venkat"],
        ["age","32"]
        ]
print(array3[0][1]) //Venkat
//Note: name title and value both are interlinking property, inorder to using that you can Struct.

//Struct syntax:
// struct MyStruct {}
// struct name is capital in the begining (MyStruct{} ex: String, Int .. all are start with capital).

//Note: intsteed of writing 2D array, we can cusomize the below way using Structures.
//Step-1:
struct Profile {
    let name = "Venkat"
    var desig = ["sr. Developer", "Mobile App Developer"]
    var skillAndYear = ["objC":7, "Swoft": 2, "react":2]
    
    //create a function with in structure
    func company() {
        print("Kyyba India")
    }
}
//Step-2: Initializing the structure
var myProfile = Profile()
print(myProfile.name)
//string interpilation
print("His name is \(myProfile.name) and designation is \(myProfile.desig[0])")

//Apend new element
myProfile.desig.append("Mobile Full stack")
print(myProfile.desig)

myProfile.company()


//Ex- 1:
struct Town {
    let name = "Venkatesh"
    var citizens = ["Venkat", "Nani"]
    var resources = ["Grain" : 100, "Ore" : 50]
    
    func fortify() {
        print("Defences Increased!")
    }
}

//Initialize
let myTown = Town()
print(myTown.name)
print(myTown.citizens)
print(myTown.resources["Grain"]!)

//String Interpilation
print("\(myTown.name) has \(myTown.resources["Grain"]!) bags off Grains")
myTown.fortify()


//Ex- 2: Dynamic Struct
struct Town1 {
    let name : String
    var citizens : [String]
    var resources :[String:Int]
    
    init(townName: String, people: [String], stats: [String: Int]) {
        name = townName
        citizens = people
        resources = stats
    }
    func fortify() {
        print("Defences Increased!")
    }
}

var anotherTown = Town1(townName: "PP", people: ["VV","NN"], stats: ["Coconuts": 100])
anotherTown.citizens.append("AA")
print(anotherTown.citizens)


//Ex- 3: Dynamic Struct with self if you declare same property name
struct Town2 {
    let name : String
    var citizens : [String]
    var resources :[String:Int]
    
    init(name: String, citizens: [String], resources: [String: Int]) {
        self.name = name
        self.citizens = citizens
        self.resources = resources
    }
    func fortify() {
        print("Defences Increased!")
    }
    
    //Error: Cannot assign through subscript: 'Self' is immutable
//    func mutableFunction() {
//        resources["Rice"] = 100
//    }
    
    mutating func mutableFunction() {
        resources["Rice"] = 100
    }
}

var anotherTown1 = Town2(name: "PP", citizens: ["VV","NN"], resources: ["Coconuts": 100])
anotherTown1.citizens.append("AA")
print(anotherTown1.citizens)

var anotherTown2 = Town2(name: "PP", citizens: ["VV","NN"], resources: ["Coconuts": 100])
anotherTown2.citizens.append("AA")
print(anotherTown2.citizens)

anotherTown2.mutableFunction()
