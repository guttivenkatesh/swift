import UIKit

//custom datatype
struct BMI {
    let value : Float
    let advice : String
    let color : UIColor
}
