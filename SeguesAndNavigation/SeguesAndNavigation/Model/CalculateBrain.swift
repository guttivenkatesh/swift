//
//  CalculateBrain.swift
//  SeguesAndNavigation
//
//  Created by VENKATESH on 16/04/20.
//  Copyright © 2020 VENKATESH. All rights reserved.
//

import UIKit

struct CalculateBrain {
    var bmi: BMI?
    
    func getBMIValu() -> String {
        //if i run this before "calculateBMI" method, app get crash because unexpectedly "bmi" have the nil value and it's trying force unwrap the nill value. so App get crash. Here you have to use optional binding.
        //let bmiTo1DecimalPlace = String(format: "%.1f", bmi!)
        
        //optional binding
//        if let safeBMI = bmi {
//            let bmiTo1DecimalPlace = String(format: "%.1f", safeBMI)
//            return bmiTo1DecimalPlace
//        }
//        else
//        {
//            return "0.0"
//        }
        
        //Nil Coalescing Operator:
//        let bmiTo1DecimalPlace = String(format: "%.1f", bmi ?? 0.0)
//        return bmiTo1DecimalPlace
        
        let bmiTo1DecimalPlace = String(format: "%.1f", bmi?.value ?? 0.0)
        return bmiTo1DecimalPlace
        
    }
    
    func getAdvice() -> String {
        return bmi?.advice ?? "No Advice"
    }
    
    func getColor() -> UIColor {
        return bmi?.color ?? UIColor.white
    }
    
    mutating func calculateBMI(height: Float, weight: Float) {
        let bmiValue = weight / pow(height, 2)
        if bmiValue < 18.5 {
            bmi = BMI(value: bmiValue, advice: "Eat more pies", color: UIColor.blue)
        }else if (bmiValue < 24.9)
        {
            bmi = BMI(value: bmiValue, advice: "Fit as a fiddle", color: UIColor.green)
        }else
        {
            bmi = BMI(value: bmiValue, advice: "Eat less pies", color: UIColor.red)
        }
        
    }
}
