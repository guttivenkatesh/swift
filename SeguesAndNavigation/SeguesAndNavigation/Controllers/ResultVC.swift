//
//  ResultVC.swift
//  SeguesAndNavigation
//
//  Created by VENKATESH on 16/04/20.
//  Copyright © 2020 VENKATESH. All rights reserved.
//

import UIKit

class ResultVC: UIViewController {

    var bmiValue : String?
    var advice : String?
    var color : UIColor?
    
    @IBOutlet weak var bmiLabel: UILabel!
    @IBOutlet weak var adviceLabel: UILabel!
    @IBAction func RecalculateAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        //dismiss(animated: true, completion: nil)// not required self, swift very smart, it will understand
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        bmiLabel.text = bmiValue
        adviceLabel.text = advice
        view.backgroundColor = color
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
