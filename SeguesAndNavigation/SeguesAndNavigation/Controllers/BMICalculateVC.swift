import UIKit

class BMICalculateVC: UIViewController {

//    var bmiValue = "0.0"
    var calculatorBrain = CalculateBrain()
    
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heightSlider: UISlider!
    @IBOutlet weak var weightSlider: UISlider!

    @IBAction func heightSliderChanged(_ sender: UISlider) {
        let height = String(format: "%.2f", sender.value)
        heightLabel.text = "\(height)m"
    }
    @IBAction func weightSliderChanged(_ sender: UISlider) {
        let weight = String(format: "%.0f", sender.value)
        weightLabel.text = "\(weight)Kg"
    }
    
    @IBAction func CalculateAction(_ sender: UIButton) {
        let height = heightSlider.value
        let weight = weightSlider.value
        
        calculatorBrain.calculateBMI(height: height, weight: weight)
        
//        let bmi = weight / pow(height, 2)
//        bmiValue = String(format: "%.1f", bmi)
        //withIdentifier means segueIdentifier from segue,
        //sender: self, self means navigating from current class
        self.performSegue(withIdentifier: "goToResult", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToResult" {
            //as! ResultVC : why we have to do this, what is that meaning we write "as"?
            //remember that we are overriding prepareforsegue method, apple created this method and we don't know that was created in ResultVC and we didn't know about "bmiValue" we need to set.
            //what they done is that "segue: UIStoryboardSegue" is initialized a box standard UIViewController, now it's our job to narrow down the datatype(segue.destination), and specify the exact datatype(as! ResultVC). by using this "as" keyword is your effectively confirming the code "downcasting". at the moment you have a UIViewController (ResultVC: UIViewController), ofcourse it's superclass of our ResultVC, we can casting down to ResultVC by writing keyword "as". and "!" is forcfully downcasting.
            //"as!" means when the segue.identifier == "goToResult" is identified then desination view controller i created
            let resultVC = segue.destination as! ResultVC
            resultVC.bmiValue = calculatorBrain.getBMIValu()
            resultVC.advice = calculatorBrain.getAdvice()
            resultVC.color = calculatorBrain.getColor()
            resultVC.bmiValue = calculatorBrain.getBMIValu()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
}
